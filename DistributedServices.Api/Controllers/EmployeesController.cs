﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Infrastructure.CrossCutting.IoC;
using DistributedServices.Entities;
using DistributedServices.Api.Mappings;
using AttributeRouting.Web.Http;
using Infrastructure.Data.MainModule.Repositories;
using Infrastructure.Data.MainModule.Models;
using System.Web.Http.Cors;

namespace DistributedServices.Api.Controllers
{
	[EnableCors(origins: "*", headers:  "*", methods:  "*")]
    public class EmployeesController : ApiController
    {
        private readonly IEmployeeRepository _service;

		private readonly IEmployeeTitleRepository _employeeTitleService;

		private readonly IEmployeeTypeRepository _employeeTypeService;

        public EmployeesController()
        {
			_service = new EmployeeRepository();

			_employeeTitleService = new EmployeeTitleRepository();

			_employeeTypeService = new EmployeeTypeRepository();
        }

        /// <summary>
        /// All of the employee titles.
        /// </summary>
        /// <returns>All employee titles.</returns>
        public HttpResponseMessage GetAll(string clientToken)
        {
            var items = _service.List(clientToken);

            var itemDto = items.Select(i => Mapper.Map(i));

            return Request.CreateResponse(HttpStatusCode.OK, itemDto);
        }


		public HttpResponseMessage Get([FromUri]int id, [FromUri]string clientToken)
		{
			var employee = _service.Get(int.Parse(id.ToString()), clientToken);

			var itemDto = Mapper.Map(employee);

			return Request.CreateResponse(HttpStatusCode.OK, itemDto);
		}

		/// <summary>
		/// A item.
		/// </summary>
		/// <param name="id">Unique identifier for an item.</param>
		/// <returns>Item.</returns>
		public HttpResponseMessage Get([FromUri]int? id, [FromUri]int? loginId, [FromUri]string clientToken)
        {

			var item = loginId != null ?
				_service.GetByLoginId(int.Parse(loginId.ToString()), clientToken) :
				_service.Get(int.Parse(id.ToString()), clientToken);

			var itemDto = Mapper.Map(item);

            return Request.CreateResponse(HttpStatusCode.OK, itemDto);
        }

        /// <summary>
        /// Creates a new employee title.
        /// </summary>
        /// <param name="item">New employee title to create in the given bundle.</param>
        /// <returns>The recently created employee title.</returns>
        public HttpResponseMessage Post([FromBody]Employee item, [FromUri]string clientToken)
        {
            if (item == null)
                return Request.CreateResponse(HttpStatusCode.OK, new Employee());

			var employeeTitle = _employeeTitleService.Get(item.TitleId, Guid.Parse(clientToken));

			var employeeType = _employeeTypeService.Get(item.EmployeeTypeId, Guid.Parse(clientToken));

			//item.EmployeeTitle = new EmployeeTitle
			//{
			//	Name = employeeTitle.Name,
			//	Description = employeeTitle.Description,
			//	Id = employeeTitle.Id
			//};

			//item.EmployeeType = new EmployeeType
			//{
			//	Name = employeeTitle.Name,
			//	Description = employeeTitle.Description,
			//	Id = employeeTitle.Id
			//};

			var itemDto = Mapper.Map(_service.Add(item, clientToken));

            return Request.CreateResponse(HttpStatusCode.OK, itemDto);
        }

        /// <summary>
        /// Updates an existing item.
        /// </summary>
        /// <param name="id">Unique identifier for the item to update.</param>
        /// <param name="item">Item to update.</param>
        /// <returns>The recently updated item.</returns>
        public HttpResponseMessage Put([FromUri]int id, [FromUri]string clientToken, [FromBody]Employee item)
        {
            if (item == null)
                return Request.CreateResponse(HttpStatusCode.OK, new Employee());

            item.Id = id;

            var itemDto = Mapper.Map(_service.Update(item, clientToken));

            return Request.CreateResponse(HttpStatusCode.OK, itemDto);
        }

		/// <summary>
		/// Updates an existing item.
		/// </summary>
		/// <param name="id">Unique identifier for the item to update.</param>
		/// <param name="item">Item to update.</param>
		/// <returns>The recently updated item.</returns>
		public HttpResponseMessage Post([FromUri]int id, [FromUri]string clientToken, [FromBody]Employee item)
		{
			if (item == null)
				return Request.CreateResponse(HttpStatusCode.OK, new Employee());

			item.Id = id;

			var itemDto = Mapper.Map(_service.Update(item, clientToken));

			return Request.CreateResponse(HttpStatusCode.OK, itemDto);
		}

		/// <summary>
		/// Deletes an existing item.
		/// </summary>
		/// <param name="id">Unique identifier for an item.</param>
		/// <returns>The recently deleted item.</returns>
		public HttpResponseMessage Delete([FromUri]int id, [FromUri]string clientToken)
		{
			var itemDto = Mapper.Map(_service.Delete(id, clientToken));

			return Request.CreateResponse(HttpStatusCode.OK, itemDto);
		}

		/// <summary>
		/// Deletes an existing item.
		/// </summary>
		/// <param name="id">Unique identifier for an item.</param>
		/// <returns>The recently deleted item.</returns>
		public HttpResponseMessage Post([FromUri]int id, [FromUri]string clientToken, [FromUri] bool delete)
        {
            var itemDto = Mapper.Map(_service.Delete(id, clientToken));

            return Request.CreateResponse(HttpStatusCode.OK, itemDto);
        }
    }
}
