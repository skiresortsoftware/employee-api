﻿using System;
using System.Collections.Generic;
using System.Linq;
using Infrastructure.Data.MainModule.Models;
using Domain.Core;
using System.Data.Entity.Validation;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Infrastructure.Data.MainModule.Mappings;

namespace Infrastructure.Data.MainModule.Repositories
{
    public class ZohoContactRepository
    {

        public ZohoContactRepository()
        {

        }

        public IEnumerable<Employee> GetAll()
        {
            var url = string.Format("https://crm.zoho.com/crm/private/json/Contacts/getMyRecords?newFormat=1&authtoken=d6a5170a464b7031f4c4be85400cd76a&scope=crmapi&fromIndex=1&toIndex=200");

            var uri = new Uri(url);


            HttpClient client = new HttpClient();

            Task<HttpResponseMessage> task = client.GetAsync(uri);

            task.Wait();

            var httpResponseMessage = task.Result;

            var response = httpResponseMessage.Content.ReadAsStringAsync().Result;

            var objResponse = JsonConvert.DeserializeObject<ResponseActual>(response);

            if (objResponse == null)
                return null;

            if (objResponse.Response == null)
                return null;

            if (objResponse.Response.Result == null)
                return null;

            if (objResponse.Response.Result.Contacts == null)
                return null;

            var employees = EmployeesMapper.Map(objResponse.Response.Result.Contacts);

            return employees;
        }


    }
}
