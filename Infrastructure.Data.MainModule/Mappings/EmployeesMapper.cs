﻿using Infrastructure.Data.MainModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.Data.MainModule.Mappings
{
    public static class EmployeesMapper
    {
        public static List<Employee> Map(Contacts contacts)
        {
            var employees = new List<Employee>();

            var rows = contacts.Row.ToList();

            foreach (var row in rows)
            {
                var employee = Map(row);

                employees.Add(employee);
            }

            return employees;
        }

        private static Employee Map(Row row)
        {
            var contact = new Employee();

            contact.Person = new Person();

            contact.EmployeeType = new EmployeeType();

            contact.EmployeeTitle = new EmployeeTitle();

            contact.Person.Gender = new Gender();

            foreach (var fl in row.FL)
            {
                if (fl.Val.ToLower() == "first name")
                    contact.Person.FirstName = fl.Content;

                if (fl.Val.ToLower() == "last name")
                    contact.Person.LastName = fl.Content;

                if (fl.Val.ToLower() == "email")
                    contact.Email = fl.Content;

                // Add try parse
                if (fl.Val.ToLower() == "date of birth")
                    contact.Person.DateOfBirth = DateTime.Parse(fl.Content);

                if (fl.Val.ToLower() == "roster id")
                    contact.RosterId = fl.Content;

                if (fl.Val.ToLower() == "current")
                    contact.Current = fl.Content.ToLower() == "true" ? true : false;

                if (fl.Val.ToLower() == "employee title")
                {
                    contact.EmployeeTitle.Name = fl.Content;
                    contact.EmployeeTitle.Description = fl.Content;
                    contact.EmployeeTitle.Id = 0;
                }

                if (fl.Val.ToLower() == "username")
                    contact.Username = fl.Content;

                if (fl.Val.ToLower() == "password")
                    contact.Password = fl.Content;

                if (fl.Val.ToLower() == "gender")
                {
                    contact.Person.Gender.Name = fl.Content;
                    contact.Person.Gender.Description = fl.Content;
                    contact.Person.Gender.Id = 0;
                }


                if (fl.Val.ToLower() == "is local")
                    contact.IsLocal = fl.Content.ToLower() == "true" ? true : false;

                if (fl.Val.ToLower() == "part time")
                {
                    contact.EmployeeType.Name = fl.Content.ToLower() == "true" ? "Part Time" : "Full Time";
                    contact.EmployeeType.Description = fl.Content.ToLower() == "true" ? "Part Time" : "Full Time";
                    contact.EmployeeType.Id = 0;
                }

                if (fl.Val.ToLower() == "is employee")
                    contact.IsEmployee = fl.Content;
            }

            if (contact.IsEmployee.ToLower() == "true")
                return contact;
            else
                return null;
        }
    }
}
