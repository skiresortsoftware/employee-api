﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Infrastructure.Data.MainModule.Repositories;
using System.Collections.Generic;
using Infrastructure.Data.MainModule.Models;

namespace Infrastructure.Data.MainModule.Test.Repositories
{
    [TestClass]
    public class ZohoContactRepositoryTest
    {
        [TestMethod]
        public void GetAll_WithNullParams_ReturnsEmployees()
        {
            // Arrange
            var repository = new ZohoContactRepository();

            // Act
            var employees = repository.GetAll();

            // Assert
            Assert.IsInstanceOfType(employees, typeof(List<Employee>));

        }
    }
}
